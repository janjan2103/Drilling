﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Drilling.Models;

namespace Drilling.Migrations
{
    [DbContext(typeof(DrillingContext))]
    partial class DrillingContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Drilling.Models.DrillingMain", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ClimateHours");

                    b.Property<DateTime>("DateOfDrilling");

                    b.Property<int?>("DrillOperatorIDID");

                    b.Property<int>("FinishHammer");

                    b.Property<int>("FinishHourmeter");

                    b.Property<int>("IdleHours");

                    b.Property<int?>("MachineIDID");

                    b.Property<int>("MaintenanceEvents");

                    b.Property<int>("MaintenanceHourmeter");

                    b.Property<int>("MaintenanceHours");

                    b.Property<int>("ProductionHours");

                    b.Property<int>("RL");

                    b.Property<int>("RedrillHours");

                    b.Property<int>("ServiceHours");

                    b.Property<int>("ShiftHours");

                    b.Property<int?>("ShiftIDID");

                    b.Property<int>("ShotNumber");

                    b.Property<int>("StandbyHours");

                    b.Property<int>("StartHamer");

                    b.Property<int>("StartHourmeter");

                    b.Property<int>("TrammingHours");

                    b.HasKey("ID");

                    b.HasIndex("DrillOperatorIDID");

                    b.HasIndex("MachineIDID");

                    b.HasIndex("ShiftIDID");

                    b.ToTable("DrillingMain");
                });

            modelBuilder.Entity("Drilling.Models.DrillingProduction", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BitNumber");

                    b.Property<string>("BitPrefix");

                    b.Property<int>("BitSharp");

                    b.Property<int?>("DrillingMainIDID");

                    b.Property<int>("Meters");

                    b.Property<int>("NumberOfHoles");

                    b.Property<string>("TypeOFHole");

                    b.HasKey("ID");

                    b.HasIndex("DrillingMainIDID");

                    b.ToTable("DrillingProduction");
                });

            modelBuilder.Entity("Drilling.Models.DrillOperator", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Operator");

                    b.Property<string>("Status");

                    b.HasKey("ID");

                    b.ToTable("DrillOperator");
                });

            modelBuilder.Entity("Drilling.Models.Equipment", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Company");

                    b.Property<string>("Fleet");

                    b.Property<string>("Status");

                    b.Property<string>("Title");

                    b.Property<string>("Type");

                    b.HasKey("ID");

                    b.ToTable("Equipment");
                });

            modelBuilder.Entity("Drilling.Models.Shift", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ShiftCod");

                    b.Property<int>("ShiftQtdHour");

                    b.HasKey("ID");

                    b.ToTable("Shift");
                });

            modelBuilder.Entity("Drilling.Models.DrillingMain", b =>
                {
                    b.HasOne("Drilling.Models.DrillOperator", "DrillOperatorID")
                        .WithMany("DrillingMain")
                        .HasForeignKey("DrillOperatorIDID");

                    b.HasOne("Drilling.Models.Equipment", "MachineID")
                        .WithMany("DrillingMain")
                        .HasForeignKey("MachineIDID");

                    b.HasOne("Drilling.Models.Shift", "ShiftID")
                        .WithMany("DrillingMain")
                        .HasForeignKey("ShiftIDID");
                });

            modelBuilder.Entity("Drilling.Models.DrillingProduction", b =>
                {
                    b.HasOne("Drilling.Models.DrillingMain", "DrillingMainID")
                        .WithMany("DrillingProduction")
                        .HasForeignKey("DrillingMainIDID");
                });
        }
    }
}
