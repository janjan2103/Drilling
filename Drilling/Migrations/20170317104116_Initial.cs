﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Drilling.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DrillOperator",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Operator = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrillOperator", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Equipment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Company = table.Column<string>(nullable: true),
                    Fleet = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipment", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shift",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ShiftCod = table.Column<int>(nullable: false),
                    ShiftQtdHour = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shift", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "DrillingMain",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClimateHours = table.Column<int>(nullable: false),
                    DateOfDrilling = table.Column<DateTime>(nullable: false),
                    DrillOperatorIDID = table.Column<int>(nullable: true),
                    FinishHammer = table.Column<int>(nullable: false),
                    FinishHourmeter = table.Column<int>(nullable: false),
                    IdleHours = table.Column<int>(nullable: false),
                    MachineIDID = table.Column<int>(nullable: true),
                    MaintenanceEvents = table.Column<int>(nullable: false),
                    MaintenanceHourmeter = table.Column<int>(nullable: false),
                    MaintenanceHours = table.Column<int>(nullable: false),
                    ProductionHours = table.Column<int>(nullable: false),
                    RL = table.Column<int>(nullable: false),
                    RedrillHours = table.Column<int>(nullable: false),
                    ServiceHours = table.Column<int>(nullable: false),
                    ShiftHours = table.Column<int>(nullable: false),
                    ShiftIDID = table.Column<int>(nullable: true),
                    ShotNumber = table.Column<int>(nullable: false),
                    StandbyHours = table.Column<int>(nullable: false),
                    StartHamer = table.Column<int>(nullable: false),
                    StartHourmeter = table.Column<int>(nullable: false),
                    TrammingHours = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrillingMain", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DrillingMain_DrillOperator_DrillOperatorIDID",
                        column: x => x.DrillOperatorIDID,
                        principalTable: "DrillOperator",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DrillingMain_Equipment_MachineIDID",
                        column: x => x.MachineIDID,
                        principalTable: "Equipment",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DrillingMain_Shift_ShiftIDID",
                        column: x => x.ShiftIDID,
                        principalTable: "Shift",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DrillingProduction",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BitNumber = table.Column<int>(nullable: false),
                    BitPrefix = table.Column<string>(nullable: true),
                    BitSharp = table.Column<int>(nullable: false),
                    DrillingMainIDID = table.Column<int>(nullable: true),
                    Meters = table.Column<int>(nullable: false),
                    NumberOfHoles = table.Column<int>(nullable: false),
                    TypeOFHole = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrillingProduction", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DrillingProduction_DrillingMain_DrillingMainIDID",
                        column: x => x.DrillingMainIDID,
                        principalTable: "DrillingMain",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DrillingMain_DrillOperatorIDID",
                table: "DrillingMain",
                column: "DrillOperatorIDID");

            migrationBuilder.CreateIndex(
                name: "IX_DrillingMain_MachineIDID",
                table: "DrillingMain",
                column: "MachineIDID");

            migrationBuilder.CreateIndex(
                name: "IX_DrillingMain_ShiftIDID",
                table: "DrillingMain",
                column: "ShiftIDID");

            migrationBuilder.CreateIndex(
                name: "IX_DrillingProduction_DrillingMainIDID",
                table: "DrillingProduction",
                column: "DrillingMainIDID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DrillingProduction");

            migrationBuilder.DropTable(
                name: "DrillingMain");

            migrationBuilder.DropTable(
                name: "DrillOperator");

            migrationBuilder.DropTable(
                name: "Equipment");

            migrationBuilder.DropTable(
                name: "Shift");
        }
    }
}
