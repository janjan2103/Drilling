using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Drilling.Models;

namespace Drilling.Controllers
{
    public class DrillingProductionsController : Controller
    {
        private readonly DrillingContext _context;

        public DrillingProductionsController(DrillingContext context)
        {
            _context = context;    
        }

        // GET: DrillingProductions
        public async Task<IActionResult> Index()
        {
            return View(await _context.DrillingProduction.ToListAsync());
        }

        // GET: DrillingProductions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var drillingProduction = await _context.DrillingProduction
                .SingleOrDefaultAsync(m => m.ID == id);
            if (drillingProduction == null)
            {
                return NotFound();
            }

            return View(drillingProduction);
        }

        // GET: DrillingProductions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DrillingProductions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,TypeOFHole,Meters,NumberOfHoles,BitPrefix,BitNumber,BitSharp")] DrillingProduction drillingProduction)
        {
            if (ModelState.IsValid)
            {
                _context.Add(drillingProduction);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(drillingProduction);
        }

        // GET: DrillingProductions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var drillingProduction = await _context.DrillingProduction.SingleOrDefaultAsync(m => m.ID == id);
            if (drillingProduction == null)
            {
                return NotFound();
            }
            return View(drillingProduction);
        }

        // POST: DrillingProductions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,TypeOFHole,Meters,NumberOfHoles,BitPrefix,BitNumber,BitSharp")] DrillingProduction drillingProduction)
        {
            if (id != drillingProduction.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(drillingProduction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DrillingProductionExists(drillingProduction.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(drillingProduction);
        }

        // GET: DrillingProductions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var drillingProduction = await _context.DrillingProduction
                .SingleOrDefaultAsync(m => m.ID == id);
            if (drillingProduction == null)
            {
                return NotFound();
            }

            return View(drillingProduction);
        }

        // POST: DrillingProductions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var drillingProduction = await _context.DrillingProduction.SingleOrDefaultAsync(m => m.ID == id);
            _context.DrillingProduction.Remove(drillingProduction);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool DrillingProductionExists(int id)
        {
            return _context.DrillingProduction.Any(e => e.ID == id);
        }
    }
}
