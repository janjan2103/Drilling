using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Drilling.Models;

namespace Drilling.Controllers
{
    public class DrillOperatorsController : Controller
    {
        private readonly DrillingContext _context;

        public DrillOperatorsController(DrillingContext context)
        {
            _context = context;    
        }

        // GET: DrillOperators
        public async Task<IActionResult> Index()
        {
            return View(await _context.DrillOperator.ToListAsync());
        }

        // GET: DrillOperators/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var drillOperator = await _context.DrillOperator
                .SingleOrDefaultAsync(m => m.ID == id);
            if (drillOperator == null)
            {
                return NotFound();
            }

            return View(drillOperator);
        }

        // GET: DrillOperators/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DrillOperators/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Operator,Status")] DrillOperator drillOperator)
        {
            if (ModelState.IsValid)
            {
                _context.Add(drillOperator);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(drillOperator);
        }

        // GET: DrillOperators/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var drillOperator = await _context.DrillOperator.SingleOrDefaultAsync(m => m.ID == id);
            if (drillOperator == null)
            {
                return NotFound();
            }
            return View(drillOperator);
        }

        // POST: DrillOperators/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Operator,Status")] DrillOperator drillOperator)
        {
            if (id != drillOperator.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(drillOperator);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DrillOperatorExists(drillOperator.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(drillOperator);
        }

        // GET: DrillOperators/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var drillOperator = await _context.DrillOperator
                .SingleOrDefaultAsync(m => m.ID == id);
            if (drillOperator == null)
            {
                return NotFound();
            }

            return View(drillOperator);
        }

        // POST: DrillOperators/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var drillOperator = await _context.DrillOperator.SingleOrDefaultAsync(m => m.ID == id);
            _context.DrillOperator.Remove(drillOperator);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool DrillOperatorExists(int id)
        {
            return _context.DrillOperator.Any(e => e.ID == id);
        }
    }
}
