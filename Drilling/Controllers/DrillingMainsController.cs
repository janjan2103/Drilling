using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Drilling.Models;

namespace Drilling.Controllers
{
    public class DrillingMainsController : Controller
    {
        private readonly DrillingContext _context;

        public DrillingMainsController(DrillingContext context)
        {
            _context = context;
        }

        // GET: DrillingMains
        public async Task<IActionResult> Index()
        {
            return View(await _context.DrillingMain.ToListAsync());
        }

        // GET: DrillingMains/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var drillingMain = await _context.DrillingMain
                .SingleOrDefaultAsync(m => m.ID == id);
            if (drillingMain == null)
            {
                return NotFound();
            }

            return View(drillingMain);
        }

        // GET: DrillingMains/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DrillingMains/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,DateOfDrilling,RL,ShotNumber,StartHourmeter,FinishHourmeter,StartHamer,FinishHammer,ShiftHours,ProductionHours,RedrillHours,TrammingHours,IdleHours,StandbyHours,ClimateHours,MaintenanceHours,ServiceHours,MaintenanceHourmeter,MaintenanceEvents")] DrillingMain drillingMain)
        {
            if (ModelState.IsValid)
            {
                _context.Add(drillingMain);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(drillingMain);
        }

        // GET: DrillingMains/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var drillingMain = await _context.DrillingMain.SingleOrDefaultAsync(m => m.ID == id);
            if (drillingMain == null)
            {
                return NotFound();
            }
            return View(drillingMain);
        }

        // POST: DrillingMains/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,DateOfDrilling,RL,ShotNumber,StartHourmeter,FinishHourmeter,StartHamer,FinishHammer,ShiftHours,ProductionHours,RedrillHours,TrammingHours,IdleHours,StandbyHours,ClimateHours,MaintenanceHours,ServiceHours,MaintenanceHourmeter,MaintenanceEvents")] DrillingMain drillingMain)
        {
            if (id != drillingMain.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(drillingMain);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DrillingMainExists(drillingMain.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(drillingMain);
        }

        // GET: DrillingMains/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var drillingMain = await _context.DrillingMain
                .SingleOrDefaultAsync(m => m.ID == id);
            if (drillingMain == null)
            {
                return NotFound();
            }

            return View(drillingMain);
        }

        // POST: DrillingMains/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var drillingMain = await _context.DrillingMain.SingleOrDefaultAsync(m => m.ID == id);
            _context.DrillingMain.Remove(drillingMain);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool DrillingMainExists(int id)
        {
            return _context.DrillingMain.Any(e => e.ID == id);
        }

        public JsonResult getDrillingProduction()
        {
            DrillingMain person = new DrillingMain
            {
            };
            return Json(person);
        }

        public JsonResult getProducts(int categoryID)
        {
        //    List<Product> products = new List<Product>();
        //    using (MyDatabaseEntities dc = new MyDatabaseEntities())
        //    {
        //        products = dc.Products.Where(a => a.CategoryID.Equals(categoryID)).OrderBy(a => a.ProductName).ToList();
        //    }
            return new JsonResult (new { });
        }

        [HttpPost]
        public JsonResult save(DrillingMain drillingMain)
        {
            DrillingMain dm = drillingMain;
            return new JsonResult (new { });
        }
    }
}
