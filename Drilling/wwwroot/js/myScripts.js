﻿var Categories = []
//fetch categories from database
function LoadCategory(element) {
    if (Categories.length == 0) {
        //ajax function for fetch data
        $.ajax({
            type: "GET",
            url: '/home/getProductCategories',
            success: function (data) {
                Categories = data;
                //render catagory
                renderCategory(element);
            }
        })
    }
    else {
        //render catagory to the element
        renderCategory(element);
    }
}

function renderCategory(element) {
    var $ele = $(element);
    $ele.empty();
    $ele.append($('<option/>').val('0').text('Select'));
    $.each(Categories, function (i, val) {
        $ele.append($('<option/>').val(val.CategoryID).text(val.CategortyName));
    })
}

//fetch products
function LoadProduct(categoryDD) {
    $.ajax({
        type: "GET",
        url: "/home/getProducts",
        data: { 'categoryID': $(categoryDD).val() },
        success: function (data) {
            //render products to appropriate dropdown
            renderProduct($(categoryDD).parents('.mycontainer').find('select.product'), data);
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function renderProduct(element, data) {
    //render product
    var $ele = $(element);
    $ele.empty();
    $ele.append($('<option/>').val('0').text('Select'));
    $.each(data, function (i, val) {
        $ele.append($('<option/>').val(val.ProductID).text(val.ProductName));
    })
}

function remove(el) {
    $(el).parent('td').parent('tr').remove();
}

$(document).ready(function () {
    //Add button click event
    $('#add').click(function () {
        //validation and add order items
        var isAllValid = true;
        if ($('#TypeOFHole').val() == "") {
            isAllValid = false;
            $('#TypeOFHole').siblings('span.error').css('visibility', 'visible');
        }
        else {
            $('#TypeOFHole').siblings('span.error').css('visibility', 'hidden');
        }
        if ($('#Meters').val() == "0") {
            isAllValid = false;
            $('#Meters').siblings('span.error').css('visibility', 'visible');
        }
        else {
            $('#Meters').siblings('span.error').css('visibility', 'hidden');
        }
        //!($('#quantity').val().trim() != '' && (parseInt($('#quantity').val()) || 0))
        if ($('#NumberOfHoles').val() == "0") {
            isAllValid = false;
            $('#NumberOfHoles').siblings('span.error').css('visibility', 'visible');
        }
        else {
            $('#NumberOfHoles').siblings('span.error').css('visibility', 'hidden');
        }
        if ($('#BitPrefix').val() == "0") {
            isAllValid = false;
            $('#BitPrefix').siblings('span.error').css('visibility', 'visible');
        }
        else {
            $('#BitPrefix').siblings('span.error').css('visibility', 'hidden');
        }
        if ($('#BitNumber').val() == "0") {
            isAllValid = false;
            $('#BitNumber').siblings('span.error').css('visibility', 'visible');
        }
        else {
            $('#BitNumber').siblings('span.error').css('visibility', 'hidden');
        }
        if ($('#BitSharp').val() == "0") {
            isAllValid = false;
            $('#BitSharp').siblings('span.error').css('visibility', 'visible');
        }
        else {
            $('#BitSharp').siblings('span.error').css('visibility', 'hidden');
        }

        if (isAllValid) {
            var $newRow = $('#mainrow').clone().removeAttr('id');
            $('.TypeOFHole', $newRow).val($('#TypeOFHole').val());
            $('.Meters', $newRow).val($('#Meters').val());
            $('.NumberOfHoles', $newRow).val($('#NumberOfHoles').val());
            $('.BitPrefix', $newRow).val($('#BitPrefix').val());
            $('.BitNumber', $newRow).val($('#BitNumber').val());
            $('.BitSharp', $newRow).val($('#BitSharp').val());

            //Replace add button with remove button
            $('#add', $newRow).addClass('remove').val('Remove').removeClass('btn-success').addClass('btn-danger').attr("onclick", 'remove(this)');

            //remove id attribute from new clone row
            $('#TypeOFHole,#Meters,#NumberOfHoles,#BitPrefix,#BitNumber,#BitSharp,#add', $newRow).removeAttr('id');
            $('span.error', $newRow).remove();
            //append clone row
            $('#drillingProductionItems').append($newRow);

            //clear select data
            $('#TypeOFHole,#Meters,#NumberOfHoles,#BitPrefix,#BitNumber,#BitSharp').val('');
            $('#drillingItemError').empty();
        } else {
            alert("Fix fields that are blank.");
        }

    });
    $('#submit').click(function () {
        var isAllValid = true;

        //validate order items
        $('#drillingProductionError').text('');
        var list = [];
        var errorItemCount = 0;
        $('#drillingProductionItems tr').each(function (index, ele) {
             var drillingProduction = {
                 TypeOFHole: $('TypeOFHole', this).val(),
                 Meters: parseInt($('.Meters', this).val()),
                 NumberOfHoles: parseInt($('.NumberOfHoles', this).val()),
                 BitPrefix: parseInt($('.BitPrefix', this).val()),
                 BitNumber: parseInt($('.BitNumber', this).val()),
                 BitSharp: parseInt($('.BitSharp', this).val())
                }
             list.push(drillingProduction);
        });
        isAllValid = true;
        if (isAllValid) {
            var data = {
                DateOfDrilling: $('#DateOfDrilling').val().trim(),
                ShotNumber: $('#ShotNumber').val().trim(),
                RL: $('#RL').val().trim(),
                StartHourMeter: $('#StartHourMeter').val().trim(),
                FinishHourMeter: $('#FinishHourMeter').val().trim(),
                StartHamer: $('#StartHamer').val().trim(),
                FinishHammer: $('#FinishHammer').val().trim(),
                MachineID: $('#MachineID').val().trim(),
                ShiftID: $('#ShiftID').val().trim(),
                DrillOperatorID: $('#DrillOperatorID').val().trim(),
                drillingProduction: list
            }

            $(this).val('Please wait...');

            $.ajax({
                type: 'POST',
                url: '/drillingMains/save',
                data: "{drillingMain:" + JSON.stringify(data) + "}",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (data) {
                    if (data.status) {
                        alert('Successfully saved');
                        //here we will clear the form
                        list = [];
                        //$('#orderNo,#orderDate,#description').val('');
                        $('#drillingProductionItems').empty();
                    }
                    else {
                        alert('Error');
                    }
                    $('#submit').text('Save');
                },
                error: function (error) {
                    console.log(error);
                    $('#submit').text('Save');
                }
            });
        }

    });

});

//LoadCategory($('#productCategory'));
