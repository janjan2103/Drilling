﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Drilling.Models;

namespace Drilling.Models
{
    public class DrillingContext : DbContext
    {
        public DrillingContext (DbContextOptions<DrillingContext> options)
            : base(options)
        {
        }

        public DbSet<Drilling.Models.DrillingMain> DrillingMain { get; set; }

        public DbSet<Drilling.Models.Shift> Shift { get; set; }

        public DbSet<Drilling.Models.Equipment> Equipment { get; set; }

        public DbSet<Drilling.Models.DrillOperator> DrillOperator { get; set; }

        public DbSet<Drilling.Models.DrillingProduction> DrillingProduction { get; set; }

    }

    //protected override void OnModelCreating(ModelBuilder modelBuilder)
    //{
    //    modelBuilder.Entity<DrillingMain>()
    //    .Property(d => d.ID)
    //    .IsRequired();

    //    modelBuilder.Entity<Shift>().ToTable("Shift");
    //    modelBuilder.Entity<Equipment>().ToTable("Equipment");
    //    modelBuilder.Entity<DrillOperator>().ToTable("DrillOperator");
    //    modelBuilder.Entity<DrillingProduction>().ToTable("DrillingProduction");
    //    modelBuilder.Entity<DrillingMain>().ToTable("DrillingMain");

    //    modelBuilder.Entity<DrillingProduction>()
    //    .HasKey(c => new { c.DrillingMainID, c.ProductionID });
    //}
}
