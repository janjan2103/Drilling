﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class Blast
    {
        public int ID { get; set; }
        public int RL { get; set; }
        public int Shot { get; set; }
        public string Letter { get; set; }
        public int VolumeBlasted { get; set; }
        public string BulkExplosiveType { get; set; }
        public int BulkExplosive { get; set; }
        public DateTime DateOfBlast { get; set; }
        public int ProdBurden { get; set; }
        public int ProdSpacing { get; set; }
        public int ProdSubdrill { get; set; }
        public int ProdStemming { get; set; }
        public int ProdDiameter { get; set; }
        public int InterRowDelay { get; set; }
        public int InterHoleDelay { get; set; }
    }
}
