﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class DrillingToolsList
    {
        public int ID { get; set; }
        public string ToolType { get; set; }
        public string Supplier { get; set; }
        public string Model { get; set; }
        public string Size { get; set; }
        public string Tool { get; set; }
    }
}
