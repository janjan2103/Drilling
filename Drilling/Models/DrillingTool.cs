﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class DrillingTool
    {
        public int ID { get; set; }
        public DateTime DateIn { get; set; }
        public string Tool { get; set; }
        public string ToolPrefix { get; set; }
        public int ToolNumber { get; set; }
        public string DrillApllied { get; set; }
        public DateTime DateOut { get; set; }
    }
}
