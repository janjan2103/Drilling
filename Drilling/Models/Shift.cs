﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class Shift
    {
        public Shift()
        {
            this.DrillingMain = new HashSet<DrillingMain>();
        }
        public int ID { get; set; }
        public int ShiftCod { get; set; }
        public int ShiftQtdHour { get; set; }
        public virtual ICollection<DrillingMain> DrillingMain { get; set; }
    }
}
