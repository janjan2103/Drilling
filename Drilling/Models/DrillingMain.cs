﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class DrillingMain
    {
        public DrillingMain()
        {
            this.DrillingProduction = new HashSet<DrillingProduction>();
        }

        public int ID { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateOfDrilling { get; set; }
        public int RL { get; set; }
        public int ShotNumber { get; set; }
        public int StartHourmeter { get; set; }
        public int FinishHourmeter { get; set; }
        public int StartHamer { get; set; }
        public int FinishHammer { get; set; }
        public int ShiftHours { get; set; }
        public int ProductionHours { get; set; }
        public int RedrillHours { get; set; }
        public int TrammingHours { get; set; }
        public int IdleHours { get; set; }
        public int StandbyHours { get; set; }
        public int ClimateHours { get; set; }
        public int MaintenanceHours { get; set; }
        public int ServiceHours { get; set; }
        public int MaintenanceHourmeter { get; set; }
        public int MaintenanceEvents { get; set; }
        public virtual Equipment MachineID { get; set; }
        public virtual Shift ShiftID { get; set; }
        public virtual DrillOperator DrillOperatorID { get; set; }
        public virtual ICollection<DrillingProduction> DrillingProduction { get; set; }
    }
}
