﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class Comment
    {
        public int ID { get; set; }
        public DateTime DateOfComments { get; set; }
        public string SafetyAndEnvironmental { get; set; }
        public string Delays { get; set; }
        public string Digging { get; set; }
        public string MajorBreakDowns { get; set; }
        public string ExtraWork { get; set; }
        public string Drilling { get; set; }
    }
}
