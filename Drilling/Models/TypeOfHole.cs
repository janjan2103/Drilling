﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class TypeOfHole
    {
        public int ID { get; set; }
        public int Title { get; set; }
        public int HoleDescription { get; set; }
    }
}
