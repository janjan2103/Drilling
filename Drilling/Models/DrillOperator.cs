﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class DrillOperator
    {
        public DrillOperator()
        {
            this.DrillingMain = new HashSet<DrillingMain>();
        }
        public int ID { get; set; }
        public string Operator { get; set; }
        public string Status { get; set; }
        public virtual ICollection<DrillingMain> DrillingMain { get; set; }
    }
}
