﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class ProductionHour
    {
        public int ID { get; set; }
        public DateTime DateOfProduction { get; set; }
        public int Shift { get; set; }
        public string Machine { get; set; }
        public int StartHourMeter { get; set; }
        public int FinishHourMeter { get; set; }
        public int ShiftHours { get; set; }
        public int ProductionHours { get; set; }
        public int NonProductionHours { get; set; }
        public int TrammingHours { get; set; }
        public int IdleHours { get; set; }
        public int StandbyHours { get; set; }
        public int ClimateHours { get; set; }
        public int MaintenanceHours { get; set; }
        public int ServiceHours { get; set; }
        public int ExtraWorkHours { get; set; }
        public int MaintenanceHourmeter { get; set; }
        public int MaintenanceEvents { get; set; }
        public int ExtraWorkCode { get; set; }
        public string ExtraWorkDescription { get; set; }
    }
}
