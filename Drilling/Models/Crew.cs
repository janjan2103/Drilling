﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class Crew
    {
        public int ID { get; set; }
        public DateTime DateOfCrews { get; set; }
        public int Shift { get; set; }
        public string Title { get; set; }
    }
}
