﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class Code
    {
        public int ID { get; set; }
        public int CodeNumber { get; set; }
        public string Description { get; set; }
    }
}
