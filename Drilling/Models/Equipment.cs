﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Drilling.Models
{
    public class Equipment
    {
        public Equipment()
        {
            this.DrillingMain = new HashSet<DrillingMain>();
        }
        public int ID { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
        public string Fleet { get; set; }
        public string Status { get; set; }
        public virtual ICollection<DrillingMain> DrillingMain { get; set; }
    }
}
