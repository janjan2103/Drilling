﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Drilling.Models
{
    public class DrillingProduction
    {
        public int ID { get; set; }
        public string TypeOFHole { get; set; }
        public int Meters { get; set; }
        public int NumberOfHoles { get; set; }
        public string BitPrefix { get; set; }
        public int BitNumber { get; set; }
        public int BitSharp { get; set; }

        public virtual DrillingMain DrillingMainID { get; set; }
    }
}
